# Surchage SVP pour gérer une mutualisation avec Git

/!\ À utiliser à vos risques et périls /!\

Ce plugin surcharge SVP pour lui permettre de mettre à jour des plugins au sein d’une mutualisation (sous réserve qu’ils aient été installés avec Git, y compris dans plugins-dist).

Il permet aussi d’installer de nouveaux plugins avec Git.
