<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip/svp.git

return [

	// S
	'svpmutu_description' => 'Ce plugin surcharge SVP pour lui permettre de mettre à jour des plugins au sein d’une mutualisation (sous réserve qu’ils aient été installés avec Git). Il permet aussi d’installer de nouveaux plugins avec Git.',
	'svpmutu_slogan' => 'Surchage SVP pour gérer une mutualisation avec Git',
];
